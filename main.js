const container = document.getElementById('container')

const buttonAdd = document.getElementById('add')
const buttonSave = document.getElementById('save')
const buttonCancel = document.getElementById('cancel')

const form = document.getElementById('form')

const formName = document.getElementById('name')
const formPrice = document.getElementById('price')
const formQuantity = document.getElementById('quantity')

const errorName = document.getElementById('errorName')
const errorPrice = document.getElementById('errorPrice')
const errorQuantity = document.getElementById('errorQuantity')

function Product(name, price, quantity) {
    this.name = name
    this.price = price
    this.quantity = quantity

    this.priceText = function () {
        return `S/ ${this.price.toFixed(2)}`
    }

    this.quantityText = function () {
        const text = this.quantity > 1 ? 'unidades disponibles' : 'unidad disponible'
        return `${this.quantity} ${text}`
    }
}

let products = []

window.onload = () => {
    let data = localStorage.getItem('products')
    if (data) {
        JSON.parse(data).forEach((item) => {
            const name = item.name
            const price = parseFloat(item.price)
            const quantity = parseInt(item.quantity)
            const product = new Product(name, price, quantity)
            addProduct(product)
        })
        createProductsList()
    }
}

buttonAdd.addEventListener('click', () => {
    showForm()
})

buttonCancel.addEventListener('click', () => {
    hideForm()
})

buttonSave.addEventListener('click', () => {
    const name = formName.value
    const price = parseFloat(formPrice.value)
    const quantity = parseInt(formQuantity.value)

    let error = false

    if (!name) {
        error = true
        showErrorName('Ingrese un nombre válido')
    } else {
        hideErrorName()
    }

    if (!price) {
        error = true
        showErrorPrice('Ingrese un precio válido')
    } else {
        hideErrorPrice()
    }

    if (!quantity) {
        error = true
        showErrorQuantity('Ingrese una cantidad válida')
    } else {
        hideErrorQuantity()
    }

    if (quantity) {
        if (quantity < 0) {
            error = true
            showErrorQuantity('Ingrese una cantidad mayor o igual a cero')
        } else {
            hideErrorQuantity()
        }
    }

    if (error) {
        return
    }

    const product = new Product(name, price, quantity)
    addProduct(product)
    saveProductsToLocalStorage()
    createProductsList()
    hideForm()
})

function getProductByName(name) {
    return products.find(product => product.name.toLowerCase() === name.toLowerCase())
}

function getProductIndexByName(name) {
    return products.findIndex(item => item.name.toLowerCase() === name.toLowerCase())
}

function addProduct(product) {
    if (getProductByName(product.name)) {
        updateProduct(product)
    } else {
        saveProduct(product)
    }
}

function saveProduct(product) {
    products.push(product)
}

function updateProduct(product) {
    const foundProductIndex = getProductIndexByName(product.name)
    if (foundProductIndex !== -1) {
        products[foundProductIndex] = product
    }
}

function showForm() {
    form.style.display = 'block'
    buttonAdd.style.display = 'none'
    formName.disabled = false

    clearForm()
    clearErrors()
}

function hideForm() {
    form.style.display = 'none'
    buttonAdd.style.display = 'block'
    formName.disabled = false

    clearForm()
    clearErrors()
}

function clearForm() {
    formName.value = ''
    formPrice.value = ''
    formQuantity.value = ''
}

function clearErrors() {
    hideErrorName()
    hideErrorPrice()
    hideErrorQuantity()
}

function showErrorName(message) {
    errorName.innerText = message
    errorName.style.display = 'block'
}

function hideErrorName() {
    errorName.innerText = ''
    errorName.style.display = 'none'
}

function showErrorPrice(message) {
    errorPrice.innerText = message
    errorPrice.style.display = 'block'
}

function hideErrorPrice() {
    errorPrice.innerText = ''
    errorPrice.style.display = 'none'
}

function showErrorQuantity(message) {
    errorQuantity.innerText = message
    errorQuantity.style.display = 'block'
}

function hideErrorQuantity() {
    errorQuantity.innerText = ''
    errorQuantity.style.display = 'none'
}

function saveProductsToLocalStorage() {
    localStorage.setItem('products', JSON.stringify(products))
}

function createProductsList() {
    container.innerHTML = ''
    if (products.length > 0) {
        for (const product of products) {
            createProductItem(product)
        }
    } else {
        container.innerHTML = '<h3>No hay productos</h3>'
    }
}

function createProductItem(product) {
    const productContainer = document.createElement('div')
    const productName = document.createElement('h2')
    const productPrice = document.createElement('p')
    const productQuantity = document.createElement('p')
    const updateButton = document.createElement('button')
    const deleteButton = document.createElement('button')

    productName.innerText = product.name
    productPrice.innerText = product.priceText()
    productQuantity.innerText = product.quantityText()

    updateButton.innerText = 'Editar'
    deleteButton.innerText = 'Eliminar'

    updateButton.onclick = updateProductItem
    deleteButton.onclick = removeProductItem

    productContainer.append(productName)
    productContainer.append(productPrice)
    productContainer.append(productQuantity)
    productContainer.append(updateButton)
    productContainer.append(deleteButton)

    container.append(productContainer)
}

function updateProductItem() {
    const name = this.parentNode.querySelector('h2').textContent
    const product = getProductByName(name)
    if (product) {
        showForm()
        formName.disabled = true
        formName.value = product.name
        formPrice.value = product.price
        formQuantity.value = product.quantity
    }
}

function removeProductItem() {
    const name = this.parentNode.querySelector('h2').textContent
    const index = getProductIndexByName(name)
    if (index !== -1) {
        products.splice(index, 1)
    }

    saveProductsToLocalStorage()
    createProductsList()
}
